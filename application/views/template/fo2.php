<div class="footer">
  <div class="pull-right">
    PT. GMF AeroAsia - Reliability Management <strong>Copyright</strong> &copy; 2019  V. 1.0
  </div>
</div>
</div>
</div>
</div>
</div>

<!--problem search -->
<script src="<?= base_url()?>assets/jq.js"></script>
<script src="<?= base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url()?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?= base_url()?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?= base_url()?>assets/js/inspinia.js"></script>
<script src="<?= base_url()?>assets/js/plugins/pace/pace.min.js"></script>
<script src="<?= base_url()?>assets/js/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?= base_url()?>assets/js/plugins/gritter/jquery.gritter.min.js"></script>
<script src="<?= base_url()?>assets/js/plugins/toastr/toastr.min.js"></script>
<script src="<?= base_url()?>assets/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?= base_url()?>assets/source/select_source.js"></script>
<script src="<?= base_url()?>assets/select2/select2.full.min.js"></script>
<script src="<?= base_url()?>assets/sweet.js"></script>
<script src="<?= base_url()?>assets/dataTables.rowsGroup.js"></script>
<script src="<?= base_url()?>assets/source/dataTables.buttons.min.js"></script>
<script src="<?= base_url()?>assets/source/buttons.flash.min.js"></script>
<script src="<?= base_url()?>assets/source/jszip.min.js"></script>
<script src="<?= base_url()?>assets/source/pdfmake.min.js"></script>
<script src="<?= base_url()?>assets/source/vfs_fonts.js"></script>
<script src="<?= base_url()?>assets/source/buttons.html5.min.js"></script>
<script src="<?= base_url()?>assets/source/buttons.print.min.js"></script>
<script src="<?= base_url()?>assets/js/datepicker.min.js"></script>
<script src="<?= base_url()?>assets/js/datepicker.en.js"></script>
<script>
  var operator = $('#operator').val();


 if (operator != '') {
  $('#actype').html('');
  var operator = $('#operator').val();
  $.ajax({
    type:"post",
    data:{'operator':operator},
    url:"<?= base_url('Search/a') ?>",
    dataType: 'json',
    success: function(data){
      var ht = '<option value=""></option>';
      $('#actype').append(ht);
      for (var i = 0; i < data.length; i++) {
       var html = '<option value = "'+data[i]['ACType']+'">'+data[i]['ACType']+'</option>';

       $('#actype').append(html);
     }
   },
 });
}

const flashData = $('.flash-data').data('flashdata');
if(flashData == 'Update') {
 Swal({
  title: 'Success !',
  text:   'Update Data Success !',
  type: 'Success'
});
} 


function io()
{
  $('#actype').html('');
  var operator = $('#operator').val();
  $.ajax({
    type:"post",
    data:{'operator':operator},
    url:"<?= base_url('Search/a') ?>",
    dataType: 'json',
    success: function(data){
      var ht = '<option value=""></option>';
      $('#actype').append(ht);
      for (var i = 0; i < data.length; i++) {
       var html = '<option value = "'+data[i]['ACType']+'">'+data[i]['ACType']+'</option>';

       $('#actype').append(html);
     }
   },
 });
}
$('#actype').select2({
  placeholder: "Select ACType",
  allowClear: true
});
$('#actype2').select2({
    placeholder: "Select ACType",
   allowClear: true
});
$('#KeyProblem').select2({
    placeholder: "Select KeyProblem",
   allowClear: true,
   tags: true
}); 
$('#ata').select2({
placeholder: "Select ATA",
   allowClear: true,
   tags: true
}); 


$('#filter').select2({
  placeholder: "Filter RTA / RTB / RTO / RTG :",
  allowClear: true
});
$('.idnsa').select2({
  allowClear: true
});
$('#operator').select2({
  placeholder: "Select Operator",
  allowClear: true
});
$('#month_from').datepicker({
  language: 'en'
});   
$('#date_from').datepicker({
  language: 'en'
});  
$('#date_to').datepicker({
  language: 'en'
});
$('#month_to').datepicker({
  language: 'en'
});



function ac()
{
  var actype = $('#actype2').val();
  $('#KeyProblem').html();
  $.ajax({
    type:"post",
    data:{'actype':actype},
    url:"<?= base_url('Search/actypeata') ?>",
    dataType: 'json',
    success: function(data){
      $('#KeyProblem').empty();
      if (data == 'Tidak Ada Table') {
        $('#KeyProblem').empty();
      } else {
       var ht = '<option value=""></option>';
       $('#KeyProblem').append(ht);
       for (var i = 0; i < data['keyword_problem'].length; i++) {
        var html = '<option value = "'+data['keyword_problem'][i]['keyword_problem']+'">'+data['keyword_problem'][i]['keyword_problem']+'</option>';
        $('#clue').val(data['nama_table']);
        $('#KeyProblem').append(html);
      } 
    }
  },
});
}
$('#table_delay').DataTable();


function atakey()
{
  var ata = $('#ata').val();
  var clue = $('#clue').val();
  if (ata == '') {
   var actype = $('#actype2').val();
   $('#KeyProblem').html();
   $.ajax({
    type:"post",
    data:{'actype':actype},
    url:"<?= base_url('Search/actypeata') ?>",
    dataType: 'json',
    success: function(data){
      $('#KeyProblem').empty();
      if (data == 'Tidak Ada Table') {
        $('#KeyProblem').empty();
      } else {
       var ht = '<option value=""></option>';
       $('#KeyProblem').append(ht);
       for (var i = 0; i < data['keyword_problem'].length; i++) {
        var html = '<option value = "'+data['keyword_problem'][i]['keyword_problem']+'">'+data['keyword_problem'][i]['keyword_problem']+'</option>';
        $('#clue').val(data['nama_table']);
        $('#KeyProblem').append(html);
      } 
    }

  },
});
 } else {
   $.ajax({
    type:"post",
    data:{'ata':ata,'clue':clue},
    url:"<?= base_url('Search/bd') ?>",
    dataType: 'json',
    success: function(data){
     if (data == 'Tidak Ada Table') {
     } else {

       $('#KeyProblem').empty();
       var ht = '<option value=""></option>';
       $('#KeyProblem').append(ht);
       for (var i = 0; i < data['keyword_problem'].length; i++) {
        var html = '<option value = "'+data['keyword_problem'][i]['keyword_problem']+'">'+data['keyword_problem'][i]['keyword_problem']+'</option>';
        $('#KeyProblem').append(html);
      } 
    }

  },
});

 }
}


</script>






