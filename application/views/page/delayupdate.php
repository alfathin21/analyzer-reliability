<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h5><i class="fa fa-pencil-square"></i>&nbsp; DELAY / UPDATE DATA</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>

        <div class="ibox-content inspinia-timeline">
          <form method="POST" action="<?= base_url('Techlog/UpdateDelay') ?>">
            <div class="row">
              <div class="form-group col-md-2">
                <label for="">ID  :</label>
                <input type="text" class="form-control" readonly="" name="notif" value="<?= $dt['ID'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">Event ID  :</label>
                <input type="text" class="form-control" name="event" value="<?=  $dt['EventID'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">STADEP :</label>
                <input type="text" class="form-control" name="stadep" value="<?= $dt['DepSta'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">STAARR :</label>
                <input type="text" class="form-control" name="staar" value="<?= $dt['ArivSta'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">ATA :</label>
                <input type="text" class="form-control" name="ata" value="<?= $dt['ATAtdm'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">SUB ATA :</label>
                <input type="text" class="form-control" name="subata" value="<?= $dt['SubATAtdm'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">Flight No :</label>
                <input type="text" class="form-control" name="fn" value="<?= $dt['FlightNo'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">RTA/RTB/RTO :</label>
                <input type="text" class="form-control" name="rta" value="<?= $dt['RtABO'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">Register :</label>
                <input type="text" class="form-control" name="reg" value="<?= $dt['Reg'] ?>">
              </div>
              <div class="form-group col-md-2">
                <label for="">A/C Type :</label>
                <select class="form-control" name="actype" id="actype2">
                  <option onchange="ac()" selected="" value="<?=  $dt['ACtype'] ?>"><?=  $dt['ACtype'] ?></option>
                  <?php foreach ($actype as $key ) :?>
                   <option value="<?=  $key['ACType'] ?>"><?=  $key['ACType'] ?></option>
                 <?php endforeach; ?>
               </select>
             </div>
             <div class="form-group col-md-2">
              <label for="">TDLength :</label>
              <input type="text" class="form-control" name="tdl" value="<?= convertToHoursMins($dt['HoursTek'],$dt['Mintek']) ?>">
            </div>
            <div class="form-group col-md-2">
              <label for="">DCP :</label>
              <input type="text" class="form-control" name="dcp" value="<?= $dt['DCP'] ?>">
            </div>
          </div>
          <div class="row">
           <div class="form-group col-md-2">
            <label for="">FDD :</label>
            <input type="text" class="form-control" name="fdd" value="<?= $dt['FDD'] ?>">
          </div>
          <div class="form-group col-md-2">
            <label for="">AOG :</label>
            <input type="text" class="form-control" name="aog" value="<?= $dt['Aog'] ?>">
          </div>

          <div class="form-group col-md-3">
            <label for="">Date Event :</label>
            <input type="date" class="form-control" name="date"  value="<?= $dt['DateEvent'] ?>">
          </div>

          <div class="form-group col-md-5">
            <label for="">KeyProblem :</label>
            <input type="text" class="form-control" name="keyproblem" value="<?= $dt['KeyProblem']?>">
          </div>

          <div class="form-group col-md-12">
            <label for="">PROBLEM :</label>
            <input type="text" class="form-control" name="problem" value="<?= $dt['Problem'] ?>">
          </div>

          <div class="form-group col-md-12">
            <label for="">Last Rectification :</label>
            <textarea  class="form-control" name="last" ><?= $dt['LastRectification'] ?></textarea>
          </div>
          <div class="form-group col-md-12">
            <label for="">Rectification / Chronology :</label>
            <textarea  class="form-control" name="action" ><?= $dt['Rectification'] ?></textarea>
          </div>
          <div class="col-md-12">
            <button class="btn btn-success" name="update"><i class="fa fa-edit"></i> &nbsp; UPDATE DATA</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>
</div>

<script>
 CKEDITOR.replace( 'action' );
 CKEDITOR.replace( 'last' );
 CKEDITOR.config.removePlugins = 'elementspath';
</script>
