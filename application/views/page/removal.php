<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
  <div class="row">
    <div class="col-md-12">
      <div class="ibox float-e-margins" id="form_query" >
        <div class="ibox-title">
          <h5><i class="fa fa-database"></i>&nbsp; Update Delay</h5>
          <div class="ibox-tools">
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content inspinia-timeline" >
         <div class="container">
           <div class="row">
             <div class="col-md-3">
              <div class="form-group">
                <label><i class="fa fa-calendar"></i>&nbsp;&nbsp;Choose the date to add data :</label>
                <input id="date_from" autocomplete="off"  data-language='en' data-date-format="yyyy-mm-dd" type="text" class="form-control">
              </div>
            </div>
            <div class="col-md-3">
              <br>
              <div class="form-group">
                <button id="delay_data" class=" btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Add Data</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5><i class="fa fa-plane"></i>&nbsp; Result Data Delay</h5>
        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content inspinia-timeline">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hovered" id="table_delay">
            <thead id ="head_cek">
              <th class="text-center">No</th>
              <th class="text-center">Event ID</th>
              <th class="text-center">Date Event</th>
              <th class="text-center">A/C Reg Orig</th>
              <th class="text-center">A/C Type</th>
              <th class="text-center">Flight No
                <th class="text-center">Sta Dep</th>
                <th class="text-center">Sta Arr</th>
                <th class="text-center">Date Insert TO</th>
                <th class="text-center">Action
                </tr></thead>
                <tbody id="enakeun">
                  <?php

                  $no = 1; 
                  foreach ($ver as $key)  :?>
                    <tr>
                      <td><?= $no ?></td>
                      <td><?= $key['EventID'] ?></td>
                      <td><?= $key['DateEvent'] ?></td>
                      <td><?= $key['Reg'] ?></td>
                      <td><?= $key['ACType'] ?></td>
                      <td><?= $key['FlightNo'] ?></td>
                      <td><?= $key['DepSta'] ?></td>
                      <td><?= $key['ArivSta'] ?></td>
                      <td><?= $key['DateInsertTO'] ?></td>
                      <td>
                        <?php $id = $key["ID"]; ?>
                        <a  href="<?= base_url() ;?>Delay/Detail?notif=<?= $key['ID']; ?>" class='btn btn-success btn-sm'><i class='fa fa-check-square'></i>&nbsp;Verification</a>
                      </td>
                    </tr>
                    <?php $no++; 
                  endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<script>
  $('#delay_data').on('click', function () {
    var date_from = $('#date_from').val();
    $.ajax({
      type:"post",
      data:{'date_from':date_from},
      url:"<?= base_url('Delay/Search') ?>",
      dataType: 'json',
      success: function(data){

        swal({
        title: "Success!",
        text: 'Data successfully added, ' + data + ' Records !',
        type: "success"
        }).then(function() {
        // Redirect the user
        window.location.href = "<?= base_url('Delay') ?>";
       
        });
    },
  });
  });


</script>



