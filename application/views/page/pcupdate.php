<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5><i class="fa fa-pencil-square"></i>&nbsp; PARETO REMOVAL / UPDATE DATA</h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					</div>
				</div>
				<form action="<?= base_url('PComponent/updatedetail') ?>" method="post">
					<div class="ibox-content inspinia-timeline">
						<div class="row">
							<div class="form-group col-md-2">
								<label for="">Notification ID  :</label>
								<input type="text" class="form-control" readonly="" name="id" value="<?= $dt['ID'] ?>">
							</div>
							<div class="form-group col-md-4">
								<label for="">Part Number :</label>
								<input type="text" class="form-control" name="part_number" value="<?= $dt['PartNo'] ?>">
							</div>
							<div class="form-group col-md-4">
								<label for="">Part Name  :</label>
								<input type="text" class="form-control" name="part_name" value="<?= $dt['PartName'] ?>">
							</div>
							<div class="form-group col-md-2">
								<label for="">Equipment :</label>
								<input type="text" class="form-control" name="equipment" value="<?= $dt['AIN'] ?>">
							</div>
							<div class="form-group col-md-5">
								<label for="">Serial Number :</label>
								<input type="text" class="form-control" name="serial_number" value="<?= $dt['SerialNo'] ?>">
							</div>
							<div class="form-group col-md-2">
								<label for="">Register :</label>
								<input type="text" class="form-control" name="reg" value="<?= $dt['Reg'] ?>">
							</div>
							<div class="form-group col-md-3">
								<label for="">A/C Type :</label>
								<input type="text" class="form-control" name="actype" value="<?= $dt['Aircraft'] ?>">
							</div>
							<div class="form-group col-md-2">
								<label for="">RemCode :</label>
								<input type="text" class="form-control" name="RemCode" value="<?= $dt['RemCode'] ?>">
							</div>
							<div class="form-group col-md-2">
								<label for="">ATA :</label>
								<input type="text" class="form-control" name="ata" value="<?= $dt['ATA'] ?>">
							</div>
							<div class="form-group col-md-3">
								<label for="">Date Removal :</label>
								<input type="date" class="form-control" name="date_removal" value="<?= $dt['DateRem'] ?>">
							</div>
							<div class="form-group col-md-7">
								<label for="">Real Reason :</label>
								<input type="text" class="form-control" name="real_reason" value="<?= $dt['real_reason'] ?>">
							</div>
							<div class="col-md-12">
								<button class="btn btn-success" name="update"><i class="fa fa-edit"></i> &nbsp; UPDATE DATA</button>
							</div>

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>


