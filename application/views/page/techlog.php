<div class="flash-data" data-flashdata="<?= $this->session->flashdata('Pesan'); ?>"></div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-md-12">
			<div class="ibox float-e-margins" id="form_query" >
				<div class="ibox-title">
					<h5><i class="fa fa-database"></i>&nbsp; Update Techlog & Verification</h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content inspinia-timeline" >
					<div class="row">
						<div class="col-md-4">
							<div class="panel panel-default">
								<div class="panel-heading">Update Techlog | Choose the date to add data</div>
								<div class="panel-body">
									<div class="form-group">
										<label for="">Date :</label>
										<input data-date-format="yyyy-mm-dd" autocomplete="off" type="text" id="date_to" class="form-control">
									</div>
									<button id="add" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add Data</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5><i class="fa fa-plane"></i>&nbsp; Analayzer Result</h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content inspinia-timeline">
					<div id="crut" class="table-responsive">
						<table class="table table-bordered table-striped table-hovered" id="data"></table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5><i class="fa fa-plane"></i>&nbsp; Aircraft Maintenance Log</h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content inspinia-timeline">
					<div id="crut" class="table-responsive">
						<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>No</th>
									<th>Notifikasi</th>
									<th>Actype</th>
									<th>REG</th>
									<th>SEQ</th>
									<th>STAARR</th>
									<th>Problem</th>
									<th>Keyword</th>
									<th>Action</th>
									<th>Category</th>
									<th>ATA</th>
									<th>SubATA</th>
								</tr>
							</thead>
							<tbody></tbody>
							<tfoot>
								<tr>
									<th>No</th>
									<th>Notifikasi</th>
									<th>Actype</th>
									<th>REG</th>
									<th>SEQ</th>
									<th>STAARR</th>
									<th>Problem</th>
									<th>Keyword</th>
									<th>Action</th>
									<th>Category</th>
									<th>ATA</th>
									<th>SubATA</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
	var table;
	$(document).ready(function() {
         //datatables
         table = $('#dataTable').DataTable({ 
         	"processing": true, 
         	"serverSide": true, 
         	"order": [], 

         	"ajax": {
         		"url": "<?= ('Techlog/get_data_ai')?>",
         		"type": "POST"
         	},
         	"columnDefs": [
         	{ 
         		"targets": [ 0 ], 
         		"orderable": false, 
         	},
         	]
         });
     });


	$("#add").on('click', function() {
		if ($.fn.dataTable.isDataTable('#data')) {
			var table = $("#data").DataTable();
			table.destroy();
			$('#data').empty();
		}

		var date = $('#date_to').val();
		var evaluation_status = '<thead id ="head_cek">' +
		'<th>Notification</th>'+
		'<th>ACTYPE</th>'+
		'<th>REG</th>'+
		'<th>FN</th>'+
		'<th>STADEP</th>'+
		'<th>STAARR</th>'+
		'<th>DATE</th>'+
		'<th>SEQ</th>'+
		'<th>DefectCode</th>'+
		'<th>ATA</th>'+
		'<th>SUBATA</th>'+
		'<th>Ata Valid</th>'+
		'<th>PROBLEM</th>'+
		'<th>ACTION</th>'+
		'<th>LengthProb</th>'+
		'<th>LengtAct</th>'+
		'<th>Corrected</th>'+
		'<th>Reason</th>'+
		'<th>PirepMarep</th>'+
		'<th>Month</th>'+
		'<tbody id="enakeun"></tbody>';


		$('#data').append(evaluation_status);
		var table = $("#data").DataTable({
			retrieve: true,
			pagin: false,
			dom: 'Bfrtip',
			buttons: [
			{extend: 'excel'}
			],
			ajax: {
				"url": "<?= base_url('Components/') ?>",
				"type": "POST",
				"data": {
					"date": date
				},
			},
			'columns': [{
				data: 'qmnum'
			},
			{
				data: 'eqart'
			},
			{
				data: 'tplnr'
			},
			{
				data: 'smr_service'
			},
			{
				data: 'smr_dep'
			},   
			{
				data: 'starr'
			},
			{
				data: 'erdat'
			},
			{
				data: 'SEQ'
			},
			{
				data: 'smr_dep'
			},
			{
				data: 'ATA'
			},
			{
				data: 'SubATA'
			},
			{
				data: 'valid'
			},
			{
				data: 'defects'
			},
			{
				data: 'action'
			},
			{
				data: 'max'
			},
			{
				data: 'max2'
			},
			{
				data: 'k'
			},
			{
				data: 'p'
			},

			{
				data: 'Category'
			},
			{
				data: 'month'
			}
			]
		});
		$('body').addClass('mini-navbar');
	});
	
</script>