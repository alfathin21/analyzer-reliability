<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
</head>
<body>

<div class="container">
	<h1>Result Table</h1>
	
<table class="table table-bordered table-striped table-hovered" id="data">
	<thead>
		<tr>
			<th>Notification</th>
			<th>ACTYPE</th>
			<th>REG</th>
			<th>FN</th>
			<th>STADEP</th>
			<th>STAARR</th>
			<th>DATE</th>
			<th>SEQ</th>
			<th>DefectCode</th>
			<th>ATA</th>
			<th>SUBATA</th>
			<th>PROBLEM</th>
			<th>ACTION</th>
			<th>LengthProb</th>
			<th>LengtAct</th>
			<th>Corrected</th>
			<th>Reason</th>
			<th>PirepMarep</th>
			<th>Month</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($response as $key ) :?>
		<tr>
			<td><?= $key['qmnum'] ?></td>
			<td><?= $key['eqart'] ?></td>
			<td><?= $key['tplnr'] ?></td>
			<td><?= $key['smr_service'] ?></td>
			<td><?= $key['smr_dep'] ?></td>
			<td><?= $key['starr'] ?></td>
			<td><?= $key['erdat'] ?></td>
			<td><?= $key['SEQ'] ?></td>
			<td><?= $key['smr_dep'] ?></td>
			<td><?= $key['ATA'] ?></td>
			<td><?= $key['SubATA'] ?></td>
			<td><?= $key['defects'] ?></td>
		
			<td><?= $key['action'] ?></td>
			<td><?= $key['max'] ?></td>
			<td><?= $key['max2'] ?></td>
			<td></td>
			<td></td>
			<td><?= $key['Category'] ?></td>
			<td><?= $key['month'] ?></td>
		<!-- 	<td><?= $key[''] ?></td>
			<td><?= $key[''] ?></td>
			<td><?= $key[''] ?></td>
			<td><?= $key[''] ?></td> -->
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
</div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready( function () {
    $('#data').DataTable();
} );
</script>

</body>
</html>