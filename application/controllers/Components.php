<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Components extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->db2 = $this->load->database('db_reprob', TRUE);
        $this->db3 = $this->load->database('db_users', TRUE);
    }
    public function index()
    {
     $date = $this->input->post('date');
     $save_date = str_replace('-','',$date);
     $query = $this->db2->query("SELECT * FROM TBL_REP_PROBLEM where erdat = '$save_date' and qmnum like 'T%'")->result_array();
    // $refer = $this->db->get('actrefer')->result_array();
     $response = [];
     foreach ($query as $key ) {
        $h['qmnum'] =  $key['qmnum'];
        $problem =  iu($key['defect']);
        $action =  iu($key['action']);
        $b = $key['smr_defcode'];
        $c = substr($key['smr_defcode'],0,2);
        $a = cekpos($key['action']);
        $list_ata = $this->db3->query("SELECT * FROM atavalid where subata = '$c' ")->row_array();
        if ($list_ata != '' or !empty($list_ata) or  !is_null($list_ata)) {
            $atafilter        = $key['smr_defcode'];
            $h['valid']       = 'Valid';
        } else {
          if ($a>0) {
            $tulisan_awal = substr($key['action'], $a);
            $angka = preg_replace("/[^0-9]/", "", $tulisan_awal);
            $ata = substr($angka,0,2);
            $subata = substr($angka,2,2);
            $defectcode = $ata.$subata;
            $list_ata = $this->db3->query("SELECT * FROM atavalid where ata = '$defectcode'  ")->row_array();
            if ($list_ata != '' or !empty($list_ata) or  !is_null($list_ata))
            {
                $atafilter = $defectcode;
                $h['valid']       = 'Valid';
            } else {
                $atafilter = 'XXXX';
                $h['valid']       = 'Tidak Valid';
            }
        } else {
            $atafilter = 'XXXX';
            $h['valid']       = 'Tidak Valid';
        }
    }

    $cekata = substr($atafilter,0,2);
    $pr3 = substr($problem, 0,3);
    if (strlen($problem) <= 22 AND strlen($action) < 22 ) 
    {
       $h['Category']  = 'NIL / Autoland';
   } 
   else {
    if (strlen($problem) <= 10 and $cekata == 'XX' ) {
        $h['Category']  = 'NIL / Autoland';
    } 
    else 
    {
        if (strlen($problem) <= 13 and  $pr3 == 'MAI' or $pr3 == 'MIA' or $pr3 == 'NIL' ) {
           $h['Category']  = 'NIL / Autoland';

       } else if (strlen($problem) <= 19 and  $pr3 == 'INSP' ) {
          $h['Category']  = 'Scheduled Job Card';
      } else {

        if (!empty(strpos($problem, 'PIN')) and !empty(strpos($action, 'REPLACE'))) {
           $h['Category']  = 'Marep';
       } else if (!empty(strpos($problem, 'NIL')) and !empty(strpos($action, 'NIL'))) {
           $h['Category']  = 'NIL / Autoland';
       } else if (!empty(strpos($problem, 'NIL')) and !empty(strpos($action, 'NOTE'))) {
         $h['Category']  = 'NIL / Autoland';
         } else if (!empty(strpos($problem, 'NIL')) and !empty(strpos($action, 'PERFORM'))) {
            $h['Category']  = 'NIL / Autoland';
        } else if (!empty(strpos($problem, 'NIL')) and !empty(strpos($action, 'PRES'))) {
           $h['Category']  = 'NIL / Autoland';
       } else if (!empty(strpos($problem, 'NIL')) and !empty(strpos($action, 'SERV'))) {
           $h['Category']  = 'NIL / Autoland';
       } else if (!empty(strpos($problem, 'PIN')) and !empty(strpos($action, 'PRES'))) {
           $h['Category']  = 'NIL / Autoland';
       } else if (!empty(strpos($problem, 'PIN')) and !empty(strpos($action, 'COVER'))) {
           $h['Category']  = 'NIL / Autoland';
       } else if (!empty(strpos($problem, 'PIN')) and !empty(strpos($action, 'PSI'))) {
           $h['Category']  = 'NIL / Autoland';
       }
           else {
// step 5
            if (!empty(strpos($problem, 'MAI')) and !empty(strpos($action, 'REF'))) {
                $h['Category']  = 'Closing Problem';
            } else if (!empty(strpos($problem, 'MAI')) and !empty(strpos($action, 'SEQ'))) {
                  $h['Category']  = 'Closing Problem';
            } else if (!empty(strpos($problem, 'MAI')) and !empty(strpos($action, 'WORK'))) {
                $h['Category']  = 'Closing Problem';
            } else if (!empty(strpos($problem, 'MAI')) and !empty(strpos($action, 'JOB'))) {
                $h['Category']  = 'Closing Problem';
            } else if (!empty(strpos($problem, 'MAI')) and !empty(strpos($action, 'CONT'))) {
                $h['Category']  = 'Closing Problem';
            } else if (!empty(strpos($problem, 'MAI')) and !empty(strpos($action, 'DURING'))) {
                $h['Category']  = 'Marep';
            } else if (!empty(strpos($problem, 'MAI')) and !empty(strpos($action, 'DRG'))) {
                $h['Category']  = 'Marep';
            } else if (!empty(strpos($problem, 'MAI')) and !empty(strpos($action, 'PIN'))) {
                $h['Category']  = 'NIL / Autoland';
            } else if (!empty(strpos($problem, 'MAI')) and !empty(strpos($action, 'PRES'))) {
                 $h['Category']  = 'NIL / Autoland';
            } else if (!empty(strpos($problem, 'MAI')) and !empty(strpos($action, 'MONITORING'))) {
                $h['Category']  = 'Scheduled Job Card';
            } else if (!empty(strpos($problem, 'INSP')) and !empty(strpos($action, 'PRES'))) {
                $h['Category']  = 'Scheduled Job Card';
            }  else if (!empty(strpos($problem, 'MAI')) and !empty(strpos($action, 'DAI'))) {
                $h['Category']  = 'Scheduled Job Card';
            }  else if (!empty(strpos($problem, 'MAI')) and !empty(strpos($action, 'SERV'))) {
                $h['Category']  = 'Scheduled Job Card';
            }  else if (!empty(strpos($problem, 'MAI')) and !empty(strpos($action, 'DATA'))) {
                $h['Category']  = 'Scheduled Job Card';
            }  else if (!empty(strpos($problem, 'MAI')) and !empty(strpos($action, 'DOC'))) {
                $h['Category']  = 'Scheduled Job Card';
            }  else if (!empty(strpos($problem, 'REF'))) {
                   $h['Category']  = 'Closing Problem';
            } else if (!empty(strpos($problem, 'HIL'))) {
                   $h['Category']  = 'Closing Problem';
            } else if (!empty(strpos($problem, 'CONT'))) {
                   $h['Category']  = 'Closing Problem';
            } else if (!empty(strpos($problem, 'SEE'))) {
                   $h['Category']  = 'Closing Problem';
            } else if (!empty(strpos($problem, 'SERVI'))) {
                   $h['Category']  = 'NIL / Autoland';
            } 
            // Step 6
             else {
                if ($pr3 == 'MAI' and !empty(strpos($action, 'PIN'))) {
                     $h['Category']  = 'NIL / Autoland';
                } else if ($pr3 == 'MAI' and !empty(strpos($action, 'PRES'))) {
                    $h['Category']  = 'Scheduled Job Card';
                } else if ($pr3 == 'INSP' and !empty(strpos($action, 'PIN'))) {
                      $h['Category']  = 'Scheduled Job Card';
                } else if ($pr3 == 'INSP' and !empty(strpos($action, 'PRES'))) {
                     $h['Category']  = 'Scheduled Job Card';
                } else if ($pr3 == 'INSP' and !empty(strpos($action, 'PSI'))) {
                     $h['Category']  = 'Scheduled Job Card';
                }
                 // Step 7 
                else {
                    if ($cekata == '25' and !empty(strpos($problem, 'LOG'))) {
                         $h['Category']  = 'Closing Problem';
                    } else if ($cekata == '38' and !empty(strpos($problem, 'LOG'))) {
                         $h['Category']  = 'Closing Problem';
                    } else if ($cekata == 'XX' and !empty(strpos($problem, 'MONITORING'))) {
                         $h['Category']  = 'Scheduled Job Card';
                    } else if ($cekata == '21' and !empty(strpos($problem, 'MONITORING'))) {
                         $h['Category']  = 'Scheduled Job Card';
                    }  else if ($cekata == '49' and !empty(strpos($problem, 'MONITORING'))) {
                         $h['Category']  = 'Scheduled Job Card';
                    }  else if ($cekata == 'XX' and !empty(strpos($problem, 'INSP'))) {
                         $h['Category']  = 'Scheduled Job Card';
                    } else if ($cekata == 'XX' and !empty(strpos($problem, 'WEIGHT'))) {
                         $h['Category']  = 'NIL / Autoland';
                    }  else if ($cekata == 'XX' and !empty(strpos($problem, 'SATIS'))) {
                         $h['Category']  = 'NIL / Autoland';
                    }  else if ($cekata == '22' and !empty(strpos($problem, 'SATIS'))) {
                         $h['Category']  = 'NIL / Autoland';
                    } 
                    // step 8
                    else {
                        if (!empty(strpos($problem, 'DMI '))) {
                          $h['Category']  = 'Closing Problem';
                        } else if (!empty(strpos($problem, 'REF HIL'))) {
                            $h['Category']  = 'Closing Problem';
                        } else if (!empty(strpos($problem, 'WO NO'))) {
                            $h['Category']  = 'Closing Problem';
                        } else if (!empty(strpos($problem, 'W/O'))) {
                            $h['Category']  = 'Closing Problem';
                        } else if (!empty(strpos($problem, 'ORDER')) and !empty(strpos($problem, '80'))) {
                            $h['Category']  = 'Closing Problem';
                        } else if (!empty(strpos($problem, 'AS')) and !empty(strpos($problem, 'HIL'))) {
                            $h['Category']  = 'Closing Problem';
                        } else if (!empty(strpos($problem, 'AS SEQ'))) {
                            $h['Category']  = 'Closing Problem';
                        } else if (!empty(strpos($problem, 'CML'))) {
                            $h['Category']  = 'Closing Problem';
                        } else if (!empty(strpos($problem, 'CABIN')) and !empty(strpos($problem, 'LOG')) ) {
                            $h['Category']  = 'Closing Problem';
                        } else if (!empty(strpos($problem, 'CABIN')) and !empty(strpos($problem, 'BOOK')) ) {
                            $h['Category']  = 'Closing Problem';
                        } else if (!empty(strpos($problem, 'PRINTER'))) {
                            $h['Category']  = 'Closing Problem';
                        }else if (!empty(strpos($problem, 'PAPER'))) {
                            $h['Category']  = 'Closing Problem';
                        }else if (!empty(strpos($problem, 'FOUND'))) {
                            $h['Category']  = 'Marep';
                        }else if (!empty(strpos($problem, 'INSP'))) {
                            $h['Category']  = 'Marep';
                        }else if (!empty(strpos($problem, 'PRACTICE'))) {
                            $h['Category']  = 'NIL / Autoland';
                        }else if (!empty(strpos($problem, 'DOCUMENT'))) {
                            $h['Category']  = 'NIL / Autoland';
                        }else if (!empty(strpos($problem, 'TAKE')) and !empty(strpos($problem, 'FOR'))) {
                            $h['Category']  = 'NIL / Autoland';
                        }else if (!empty(strpos($problem, 'STILL')) and !empty(strpos($problem, 'EXIST'))) {
                            $h['Category']  = 'Pirep';
                        }else if (!empty(strpos($problem, 'MAINT')) and !empty(strpos($problem, 'STATUS')) ) {
                            $h['Category']  = 'Pirep';
                        }else if (!empty(strpos($problem, 'MAINT')) and !empty(strpos($problem, 'STS')) ) {
                            $h['Category']  = 'Pirep';
                        } 
                        // step 9
                         else {
                                if (strlen($problem) >= 44 and $pr3 == 'MAI') {
                                    $h['Category']  = 'Marep';
                                }
                                // step 10 
                                else {
                                    if ($cekata == 'XX' and empty(strpos($problem, 'REF')) ) {
                                       $h['Category']  = 'NIL / Autoland';
                                    } else if ($cekata == 'XX' and empty(strpos($problem, 'MONITOR'))) {
                                         $h['Category']  = 'NIL / Autoland';
                                    } else if ($cekata == 'XX' and empty(strpos($problem, 'NEXT'))) {
                                        $h['Category']  = 'NIL / Autoland';
                                    }
                                }
                        }
                    }   
                }
            }  
        }
}
}
}
$jumlah_smr_defcode = strlen($atafilter);
$ATA =  $cekata;
$SubATA = $atafilter;
$STAARR = substr($key['arbplwerk'],0,3);
if ($STAARR == 'GAH') {
    $STAARR = "CGK";
}
$h['starr']  = $STAARR;
$h['smr_defcode'] = $atafilter;
$h['tplnr'] =  $key['tplnr'];
$h['begru'] =  $key['begru'];
$h['eqart'] =  $key['eqart'];
$h['qmart'] =  $key['qmart'];
$h['qmdat'] =  $key['qmdat'];
$h['strmn'] =  $key['strmn'];
$h['qmcod'] =  $key['qmcod'];
$h['defects'] =  iu($key['defect']);
$h['action'] =  iu($key['action']);
$h['arbplwerk'] =  $key['arbplwerk'];
$h['arbpl'] =  $key['arbpl'];
$h['status'] =  $key['status'];
$h['idx'] =  $key['idx'];
$h['smr_dep'] =  $key['smr_dep'];
$h['smr_service'] =  $key['smr_service'];
$h['erdat'] =  $key['erdat'];
$h['aedat'] =  $key['aedat'];
$h['matnr_in'] =  $key['matnr_in'];
$h['sernr_in'] =  $key['sernr_in'];
$h['matnr_out'] =  $key['matnr_out'];
$h['sernr_out'] =  $key['sernr_out'];
$h['max']       = strlen($problem);
$h['max2']    =  strlen($action);
$h['SEQ']    = substr($key['qmnum'],-4,2);
$h['ATA']   = $ATA;
$h['SubATA'] = $SubATA;
$h['month'] =  substr($key['qmdat'],0,6);
$h['k'] = '-';
$h['p'] = '-';
array_push($response,$h);
}

$newresponse = array(
    'data' => $response
);
echo json_encode($newresponse);

}

}

/* End of file Pareto.php */
/* Location: ./application/controllers/Pareto.php */