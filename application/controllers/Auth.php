<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Techlog_model');
		date_default_timezone_set('Asia/Jakarta');
		$this->load->library('form_validation');
		$this->load->library('user_agent');
		$this->load->helper('cookie');
		$this->db_users = $this->load->database('db_users', TRUE);
		error_reporting(0);
	}
	public function index()
	{
		$this->form_validation->set_rules('uname', 'username', 'required|trim',[
			'required' => 'Harap isi kolom ini',
		]);
		$this->form_validation->set_rules('pass', 'Password', 'required|trim');
		if ($this->form_validation->run() == FALSE) {
			
			$this->load->view('page/login');
		} else {
			$this->_login();
		}
	}

	private function _login()
	{
		if ($this->agent->is_browser())
		{
			$agent = $this->agent->browser() . ' ' . $this->agent->version();
		}
		elseif ($this->agent->is_robot())
		{
			$agent = $this->agent->robot();
		}
		elseif ($this->agent->is_mobile())
		{
			$agent = $this->agent->mobile();
		}
		else
		{
			$agent = 'Unidentified User Agent';
		}
		$username = htmlspecialchars($this->input->post('uname'));
		$password = htmlspecialchars($this->input->post('pass'));
		$user = $this->db_users->get_where('users', ['username' => $username])->row_array();
		$ipAddress = $_SERVER['REMOTE_ADDR'];
		$platform = $this->agent->platform();
		if ($user) {
			if (password_verify($password, $user['password'])) {
				$insert_db = 
				[
					'log_description' => $username .' '. 'Login success ',
					'datetime' => date('Y-m-d H:i:s'),
					'id' => $username,
					'ip_address' => $ipAddress,
					'user_agent'  => $platform,
					'apps'			=> 'Dashboard'
				];
				$row = $this->Techlog_model->getDataSession($username);
				$data  = 
				[
					
					'nama'      => $row['nama'], 
					'nopeg'     => $row['nopeg'],
					'email'     => $row['email'],
					'unit'	    => $row['unit'],
					'is_active' => 1,
					'role_id'   => $row['role_id'],
					'unit'      => $row['unit'],
					'apps'		=> 'Dashboard'
				];
				$this->session->set_userdata($data);
				$this->Techlog_model->savelog($insert_db);
				redirect('Techlog');

			}else {
				$insert_db = 
				[
					'log_description' => $username .' '. 'Login failed ',
					'datetime' => date('Y-m-d H:i:s'),
					'id' => $username,
					'ip_address' => $ipAddress,
					'user_agent'  => $platform,
					'apps'		=> 'Dashboard' 
				];
				$this->Techlog_model->savelog($insert_db);
				$this->session->set_flashdata("Pesan", "Password is wrong !");
				redirect(base_url());
			}

		} else {
			$dn = "DC=gmf-aeroasia,DC=co,DC=id";
			$ldapconn 	= ldap_connect("192.168.240.66") or die ("Could not connect to LDAP server.");
			if (@$ldapconn)
			{
				ldap_set_option(@$ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
				ldap_set_option(@$ldapconn, LDAP_OPT_REFERRALS, 0);
				$ldapbind = ldap_bind($ldapconn, "ldap", "aeroasia");         
				if ($ldapbind)
				{
					@$sr = ldap_search($ldapconn, $dn, "samaccountname=$username");
					@$info = ldap_get_entries($ldapconn, @$sr);
					if (@$info[0]["samaccountname"][0] == $username)
					{
						@$bind = @ldap_bind($ldapconn, $info[0]['dn'], $password);
						if (!$bind || !isset($bind))
						{
							$insert_db = 
							[
								'log_description' => $username .' '. 'Login failed ',
								'datetime' => date('Y-m-d H:i:s'),
								'id' => $username,
								'ip_address' => $ipAddress,
								'user_agent'  => $agent . $platform ,
								'apps'		=> 'Dashboard'
							];
							$ins = $this->Techlog_model->savelog($insert_db);
							if ($ins) {
								$this->session->set_flashdata("Pesan", "Password Wrong !");
								$this->db_users->insert('logreliability', $insert_db);
								redirect(base_url());
							}
						} else {
							@$name = $info[0]["cn"][0];
							@$nopeg = $info[0]['samaccountname'][0];
							@$email = $info[0]['mail'][0];
							@$unit = $info[0]['department'][0];
							@$jabatan = $info[0]['title'][0];
							$row = $this->Techlog_model->getDataSession($nopeg);
							if (is_null($row)) {
								$data = 
								[
									'nama' => $name,
									'nopeg' => $nopeg,
									'email'	=> $email,
									'unit'	=> $unit,
									'jabatan' => $jabatan,
									'is_active' => 1,
									'role_id'	=> 8
								];
								$this->Techlog_model->saveuserdetail($data);
								$insert_db = 
								[
									'log_description' => $username .' '. 'success login',
									'datetime' => date('Y-m-d H:i:s'),
									'id' => $username,
									'ip_address' => $ipAddress,
									'user_agent'  => $agent . $platform,
									'apps'		=> 'Dashboard'
								];
								$this->session->set_userdata($data);
								$this->Techlog_model->savelog($insert_db);
								redirect('Techlog','refresh');
							} else {
								$data  = 
								[
									
									'nama' =>  $row['nama'], 
									'nopeg' => $row['nopeg'],
									'email' => $row['email'],
									'unit'	=> $row['unit'],
									'is_active' => 1,
									'role_id' => $row['role_id']
								];
								$insert_db = 
								[
									'log_description' => $username .' '. 'success login',
									'datetime' => date('Y-m-d H:i:s'),
									'id' => $username,
									'ip_address' => $ipAddress,
									'user_agent'  => $agent . $platform,
									'apps'		=> 'Dashboard'
								];

								$this->session->set_userdata($data);
								$this->Techlog_model->savelog($insert_db);
							
							
								redirect('Techlog','refresh');
								

							}

						} 
					}

					else {
						$insert_db = 
						[
							'log_description' => $username .' '. 'Login failed invalid username and password ',
							'datetime' => date('Y-m-d H:i:s'),
							'id' => $username,
							'ip_address' => $ipAddress,
							'user_agent'  => $agent . $platform,
							'apps'		=> 'Dashboard'
						] ;

						$this->db_users->insert('logreliability', $insert_db);
						$this->session->set_flashdata("Pesan", "Unregistered username");
						redirect(base_url());
					}

				} else {
					$insert_db = 
					[
						'log_description' => $username .' '. 'Login failed invalid username and password ',
						'datetime' => date('Y-m-d H:i:s'),
						'id' => $username,
						'ip_address' => $ipAddress,
						'user_agent'  => $agent . $platform,
						'apps'		=> 'Dashboard' 
					] ;

					$this->db_users->insert('logreliability', $insert_db);
					$this->session->set_flashdata("Pesan", "Username & Password is wrong");
					redirect(base_url());
				}
			}
		}

	}

	public function logout()
	{
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('nopeg');
		$this->session->unset_userdata('nama');
		$this->session->unset_userdata('unit');
		delete_cookie('User_cookies');
		$this->session->set_flashdata("Pesan", "Logout success");
		redirect(base_url());
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */