<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
	}

	public function a()
	{
		$p = $this->input->post("operator");
		$sa = $this->db->query("SELECT DISTINCT ACType from tbl_masterac where Operator = '$p' ORDER BY ACType ASC ")->result_array();
		echo json_encode($sa);
	}


	public function ata()
	{
		$p = $this->input->post("ata");
		$sa = $this->db->query("SELECT ATA_DESC from tbl_master_ata where ATA = '$p' ")->result_array();

		echo json_encode($sa);
	}

	public function actypeata()
	{
		$p = $this->input->post("actype");


		$o = str_replace(' ', '_', $p);
			$c = str_replace('-','_',$o);


			
			$nama_table = "keyword_".strtolower($c);
		
		$table_keyword = 
		[
			"keyword_crj_1000","keyword_b777_300","keyword_b737_max","keyword_b737_800","keyword_atr72_600",
			"keyword_a330_300","keyword_a330_200","keyword_a320_200","keyword_crj_1000"
		]; 

		if ($p == "CRJ-1000") {
			$b = $this->db->query("SELECT DISTINCT keyword_problem FROM keyword_crj_1000 ")->result_array();
	
				$new_response = 
				[
					"keyword_problem" => $b,
					"nama_table" => 'keyword_crj_1000'
				
				];
				echo json_encode($new_response);
			
		} else {
			$d = $nama_table;
			$cek =  array_search($d, $table_keyword);
			if ($cek != '') {
				$b = $this->db->query("SELECT DISTINCT keyword_problem FROM $d ")->result_array();
	
				$new_response = 
				[
					"keyword_problem" => $b,
					"nama_table" => $d
				
				];
				echo json_encode($new_response);
				
			} else {
				$new_response = "Tidak Ada Table";
				echo json_encode($new_response);
			}
			//echo "Tidak Masuk";

		}
			
	}

	public function listata()
	{
		$sa = $this->db->query("SELECT ATA from tbl_master_ata")->result_array();
		echo json_encode($sa);
	}

	public function bd()
	{
		$clue = $this->input->post("clue");
		$ata = $this->input->post("ata");
	
		if ($clue != '') {
			$c = "SELECT DISTINCT keyword_problem FROM $clue where ata = '$ata'";
			$b = $this->db->query("SELECT DISTINCT keyword_problem FROM $clue where ata = '$ata' ")->result_array();
			$new_response = 
			[
				"keyword_problem" => $b,
				"query" =>$c
			];
			echo json_encode($new_response);
		} else {
			echo json_encode("Tidak ada table");
		}

	}

}

/* End of file Search.php */
/* Location: ./application/controllers/Search.php */