<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delay extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		is_log_in();
		$this->load->model('Techlog_model');
		$this->db_tdam = $this->load->database('db_tdam', TRUE);
		error_reporting(0);
		
	}
	function index()
	{
		$data['title'] = 'Analyzer Delay';
		$verify = "SELECT ID,EventID,DateEvent,ACType,Reg,FlightNo,DepSta,ArivSta,DateInsertTO FROM mcdrnew WHERE status_review = 'UNREAD' ORDER BY DateInsertTO DESC  ";
			$hasil_akhir = $this->db->query($verify)->result_array();
		$data['ver'] = $hasil_akhir;
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/removal');
		$this->load->view('template/fo2');
	}

	function Search()
	{
		$waktu = $this->input->post('date_from');
		$result = "SELECT EventID,DateDeptEvent,ACTypeOrig,ACRegOrig,FltNo,StaDep,StaArr,atachap,sub_atachap,ttl_delay_lgth,ttl_tdelay_lgth,ffod1,Note,IATACode,DateEvent,UpdateDate,InsertDate,DCP FROM tblevent WHERE InsertDate LIKE '%$waktu%' ";
		$hasil = $this->db_tdam->query($result)->result_array();
		$tot = $this->db_tdam->query($result)->num_rows();
		$no = 1;
		$response = [];
		$UpdateDateTER = date('Y-m-d');
		foreach ($hasil as $key) {
		    $event = $key["EventID"];
			$result2 = "SELECT DescriptionRect FROM tblmcrectdata WHERE EventID = '$event' ";
			$ttl_delay_lgth = convertToHours($key['ttl_delay_lgth'], '%02d');
			$mintot = convertToMins($key['ttl_delay_lgth'], '%02d');
			$ttl_tdelay_lgth = convertToHours($key['ttl_tdelay_lgth'], '%02d');
			$mintek = convertToMins($key['ttl_tdelay_lgth'], '%02d');
			$description = $this->db_tdam->query($result2)->result_array();
			$b = json_encode($description);
			$uang = str_replace('[{"DescriptionRect":"', '', $b);
			$uang2 = str_replace('"},{"DescriptionRect":"', '', $uang);
			$uang3 = str_replace('"}]', '', $uang2);
			$data = 
			[
				"ID" => '',
				"DateEvent" => $key["DateEvent"],
				"ACType" => cekactype($key["ACRegOrig"]),
				"Reg" => $key["ACRegOrig"],
				"FlightNo" => $key["FltNo"],
				"DepSta" => $key["StaDep"],
				"ArivSta" => $key["StaArr"],
				"ATAtdm" => $key["atachap"],
				"SubATAtdm" => $key["sub_atachap"],
				"HoursTot" => $ttl_delay_lgth,
				"MinTot" => $mintot,
				"HoursTek" => $ttl_tdelay_lgth,
				"Mintek" => $mintek,
				"EventID" => $event,
				"FDD" => $key["ffod1"],
				"Problem" => $key["Note"],
				"DCP" => $key["DCP"],
				"Iata" => $key["IATACode"],
				"UpdateDateTER" => $UpdateDateTER,
				"CreateDateSwift" => $key["DateEvent"],
				"Rectification" => $uang3,
				"UpdateDateTO" => $key["InsertDate"],
				"DateInsertTO" => $key["InsertDate"],
				"status_review" => 'UNREAD'
			];

			$this->db->insert('mcdrnew', $data);
			
		}
 echo json_encode($tot);
	}
	function Detail()
	{
		$data['title'] = 'Detail Verification';
		$data['actype'] = $this->Techlog_model->getactype();
		$notif = $this->input->get('notif');
		$data['ata'] = $this->Techlog_model->getata();
			$data['dt'] = $this->db->query("SELECT ID, ACtype as ac,DateEvent,EventID,ACtype, Reg, DepSta, ArivSta,KeyProblem,FlightNo,HoursTek,Mintek, ATAtdm,FDD,Aog, SubATAtdm, Problem,Rectification, DCP, RtABO, MinTot, LastRectification AS last, status_review FROM mcdrnew WHERE ID = '$notif' ")->row_array();
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/pirep');

		$this->load->view('template/fo2');
	}

   public function UpdateDelay()
	{

		$notif = $this->input->post("notif");
		$event = $this->input->post("event");
		$stadep = $this->input->post("stadep");
		$staar = $this->input->post("staar");
		$ata = $this->input->post("ata");
		$subata = $this->input->post("subata");
		$fn = $this->input->post("fn");
		$rta = $this->input->post("rta");
		$reg = $this->input->post("reg");
		$actype = $this->input->post("actype");
		$date = $this->input->post("date");
		$dcp = $this->input->post("dcp");
		$rta = $this->input->post("rta");
		$problem = $this->input->post("problem");
		$action = htmlspecialchars($this->input->post("action"));
		$tdl = $this->input->post("tdl");
		$last = htmlspecialchars($this->input->post("last"));
		$keyproblem = $this->input->post("keyproblem");
		$fdd = $this->input->post("fdd");
		$aog = $this->input->post("aog");
		$clue = $this->input->post("clue");
		$user = $this->session->userdata('nama');


if (!empty($clue)) {
	//$this->db->query("SELECT * FROM $clue where ata = '$ata' AND keyword_problem = '$keyproblem' ")->result_array();
	$a = "SELECT * FROM $clue where ata = '$ata' AND keyword_problem = '$keyproblem'";
	$pengecekan = $this->db->query($a)->result_array();
	if ($pengecekan == '' or empty($pengecekan)) {
		$data = 
		[
			"id" => '',
			"ata" => $ata,
			"keyword_problem" => $keyproblem
		];
		$this->db->insert($clue, $data);
	}

}


		if (isset($_POST['save'])) {
			$this->db->set('ACType',$actype);
			$this->db->set('Reg',$reg);
			$this->db->set('FlightNo',$fn);
			$this->db->set('DepSta',$stadep);
			$this->db->set('ArivSta',$staar);
			$this->db->set('DCP',$dcp);
			$this->db->set('EventID',$event);
			$this->db->set('DateEvent',$date);
			$this->db->set('ATAtdm',$ata);
			$this->db->set('SubATAtdm',$subata);
			$this->db->set('Problem',$problem);
			$this->db->set('RtABO',$rta);
			$this->db->set('Rectification',$action);
			$this->db->set('LastRectification',$last);
			$this->db->set('KeyProblem',$keyproblem);
			$this->db->set('FDD',$fdd);
			$this->db->set('Aog',$aog);
			$this->db->where('ID', $notif);
			$this->db->update('mcdrnew');		
		} else if (isset($_POST['update'])) {
			$this->db->set('ACType',$actype);
			$this->db->set('Reg',$reg);
			$this->db->set('FlightNo',$fn);
			$this->db->set('DepSta',$stadep);
			$this->db->set('ArivSta',$staar);
			$this->db->set('DCP',$dcp);
			$this->db->set('EventID',$event);
			$this->db->set('DateEvent',$date);
			$this->db->set('ATAtdm',$ata);
			$this->db->set('SubATAtdm',$subata);
			$this->db->set('Problem',$problem);
			$this->db->set('RtABO',$rta);
			$this->db->set('Rectification',$action);
			$this->db->set('LastRectification',$last);
			$this->db->set('KeyProblem',$keyproblem);
			$this->db->set('FDD',$fdd);
			$this->db->set('Aog',$aog);
			$this->db->set('status_review','REVIEW');
			$this->db->set('user_review', $user);
			$this->db->where('ID', $notif);
			$this->db->update('mcdrnew');
		}

	
		$this->session->set_flashdata("Pesan", "Update");
		redirect('Delay','refresh');

	}




	
}

/* End of file PComponent.php */
/* Location: ./application/controllers/PComponent.php */