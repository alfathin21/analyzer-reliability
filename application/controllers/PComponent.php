<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delay extends CI_Controller {
	 function __construct()
	{
		parent::__construct();
		is_log_in();
		$this->load->model('Techlog_model');
		
	}
	 function index()
	{
		$data['title'] = 'Analyzer Delay';
		$data['unit'] = $this->session->userdata('unit');
		$data['ata'] = $this->Techlog_model->getata();
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/removal');
		$this->load->view('template/fo2');
	}
	
}

/* End of file PComponent.php */
/* Location: ./application/controllers/PComponent.php */