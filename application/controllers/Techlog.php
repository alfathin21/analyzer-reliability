<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Techlog extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		is_log_in();
		$this->load->model('Techlog_model');	
		$this->load->model('Ai_model');	
	}
	function index()
	{
		$data['title'] = 'Verification Analyzer';
		// $data['ni'] = $this->Techlog_model->getTechlog();
		$this->load->view('template/head',$data);
		$this->load->view('template/side');
		$this->load->view('page/techlog');
		$this->load->view('template/fo2');
	}
	 function get_data_ai()
	{
		$list = $this->Ai_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$angka = 1;
		foreach ($list as $field) {
			$row = array();
			$row[] = $angka;
			
			$row[] = $field['Notification'];
			$row[] = $field['ACTYPE'];
			$row[] = $field['REG'];
			$row[] = $field['SEQ'];
			$row[] = $field['STAARR'];
			$row[] = $field['PROBLEM'];
			$row[] = $field['Keyword'];
			$row[] = $field['ACTION'];
			$row[] = $field['PirepMarep'];
			$row[] = $field['ATA'];
			$row[] = $field['SUBATA'];
			$data[] = $row;
			$no++;
			$angka++;
		}

		$output = array( 
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Ai_model->count_all(),
			"recordsFiltered" => $this->Ai_model->count_filtered(),
			"data" => $data,
		);
        //output dalam format JSON
		echo json_encode($output);
	}



}
