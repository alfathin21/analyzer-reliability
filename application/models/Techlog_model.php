<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Techlog_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->db_users = $this->load->database('db_users', TRUE);
		$this->db2 = $this->load->database('db_reprob', TRUE);
	}
	function getTechlog() 
	{
		$query = $this->db->query("SELECT * FROM tblpirep_swift order by Created_on DESC LIMIT 100");
		return $query->result();
	}
	public function getDataSession($username)
	{
		return $this->db_users->query("SELECT * FROM users_detail WHERE nopeg = '$username' ")->row_array();
	}
	public function savelog($data)
	{
		return $this->db_users->insert('logreliability', $data);
	}
	public function getactype()
	{
		return $this->db->query("SELECT ACType FROM tbl_master_actype")->result_array();
	}	
	public function getata()
	{
		return $this->db->query("SELECT DISTINCT ATA FROM tbl_master_ata")->result_array();
	}	
	public function getdesc()
	{
		return $this->db->query("SELECT DISTINCT ATA_DESC FROM tbl_master_ata")->result_array();
	}


}

/* End of file Techlog_model.php */
/* Location: ./application/models/Techlog_model.php */